export function fight(firstFighter, secondFighter) {
    const firstTurn = Math.random() * 10;
    return firstTurn < 5 ? processBattle(firstFighter, secondFighter)
        : processBattle(secondFighter, firstFighter);
}

export function processBattle(attacker, enemy) {
    while (true) {
        if (attacker.health <= 0) {
            return enemy;
        }
        enemy.health -= getDamage(attacker, enemy);

        if (enemy.health <= 0) {
            return attacker;
        }
        attacker.health -= getDamage(enemy, attacker);
    }
}

export function getDamage(attacker, enemy) {
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(enemy);
    const damage = hitPower - blockPower;
    return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
    const criticalHitChance = Math.random() + 1;
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance = Math.random() + 1;
    return fighter.defense * dodgeChance;
}
