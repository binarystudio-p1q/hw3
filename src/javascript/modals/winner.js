import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
    const title = 'THE WINNER IS:';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
    const { source, name } = fighter;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

    const nameElement = createElement({ tagName: 'h1', className: 'fighter-name' });
    nameElement.innerText = name;

    const sourceAttribute = { src: source };
    const imageElement = createElement({ tagName: 'img', attributes: sourceAttribute });

    fighterDetails.append(nameElement, imageElement);
    return fighterDetails;
}
