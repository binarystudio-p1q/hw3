import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { source, name } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const nameElement = createElement({ tagName: 'h1', className: 'fighter-name' });
  nameElement.innerText = name;

  const attack = createDataElement('Attack', fighter.attack);
  const defense = createDataElement('Defense', fighter.defense);
  const health = createDataElement('Health', fighter.health);

  const sourceAttribute = { src: source };
  const imageElement = createElement({ tagName: 'img', attributes: sourceAttribute });

  fighterDetails.append(nameElement, attack, defense, health, imageElement);
  return fighterDetails;
}

function createDataElement(name, data) {
  const element = createElement({ tagName: 'div'});
  const nameElement = createElement({tagName: 'span'});
  nameElement.innerText = `${name}: `;

  const dataElement = createElement({tagName: 'span'});
  dataElement.innerText = data;

  element.append(nameElement, dataElement);
  return element;
}
